﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Racer{
    class House
    {
        Point housePosition;
      Texture2D imgHouse;
      int decalY;
      double animElapsed = 0;
        public House(Point position)
        {
            Random rnd = new Random();
            int houseType=rnd.Next(0, 36);
           imgHouse= GameStatics.content.Load<Texture2D>("Sprites/house"+houseType.ToString());
           housePosition = position;
            decalY = housePosition.Y;
        }
        public void Update(int speed)
           
        {
            
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
               
                    decalY +=  speed ;
               
            }
            housePosition.Y = decalY;

        }
        public void Draw()
        {
           
            GameStatics.spriteBatch.Draw(imgHouse,new Rectangle(housePosition.X, housePosition.Y, imgHouse.Width, imgHouse.Height),Color.White);
    
        }
        public Rectangle getArea()
        {
            return new Rectangle(housePosition.X,housePosition.Y,imgHouse.Width,imgHouse.Height);
        }
        public Point getPosition()
        {
           
            return housePosition;
        }
    }
}
