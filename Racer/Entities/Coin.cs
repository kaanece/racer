﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Racer
{
    class Coin
    {
        Point coinPosition;
        Texture2D imgCoin;
        int decalY;
        double animElapsed = 0;
        public Coin(Point position)
        {
            coinPosition = position;
            imgCoin = GameStatics.content.Load<Texture2D>("Sprites/coin");
            decalY = coinPosition.Y;
           
        }
        public void Update(int speed)
        {

            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                decalY += speed;
            }
            coinPosition.Y = decalY;
        }
        public void Draw()
        {
         
            GameStatics.spriteBatch.Draw(imgCoin, new Rectangle(coinPosition.X, coinPosition.Y, 30, 30), Color.White);
        }
        public Rectangle getArea()
        {
            return new Rectangle(coinPosition.X, coinPosition.Y, 30, 30);
        }
    }
}
