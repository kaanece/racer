﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Racer
{
    class PlayerCar
    {
        Point carPosition;
        Texture2D imgCar;
        Texture2D imgHealth;
        Texture2D imgDeath;
        int health;
        bool crash = false;
        int crashTime;
        int fuel;
        double nextSecond;
        public PlayerCar()
        {
            if(GameStatics.maincar==0)
            imgCar = GameStatics.content.Load < Texture2D>("Sprites/maincar0");
            else if (GameStatics.maincar == 1)
                imgCar = GameStatics.content.Load<Texture2D>("Sprites/maincar1");
            else if (GameStatics.maincar == 2)
                imgCar = GameStatics.content.Load<Texture2D>("Sprites/maincar2");
            imgHealth = GameStatics.content.Load<Texture2D>("Sprites/health");
            imgDeath = GameStatics.content.Load<Texture2D>("Sprites/dead");
            carPosition = new Point(485, 600);
            health = 5;
            fuel = 200;
            nextSecond = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + 2;

        }
        public void Update()
        {
            if (crash)
            {
                if (crashTime == (int)GameStatics.gameTime.TotalGameTime.TotalSeconds)
                    crash = false;
              
            }
           if(Keyboard.GetState().IsKeyDown(Keys.Up))
           {
               if(carPosition.Y>0)
               carPosition.Y -= 2;
           }
           if (Keyboard.GetState().IsKeyDown(Keys.Down))
           {
               if(carPosition.Y<640)
               carPosition.Y += 4;
           }
           if (Keyboard.GetState().IsKeyDown(Keys.Left))
           {
                 if(carPosition.X>0)
                   carPosition.X -= 4;
           }
           if (Keyboard.GetState().IsKeyDown(Keys.Right))
           {
               if(carPosition.X<1000)
               carPosition.X += 4;
           }
           if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextSecond)
           {
               fuel-=5;
               nextSecond++;
           }
           if (fuel <= 0)
           {
               decreaseHealth();
               GameStatics.gameScore -= 50;
           }

        }
        public void Draw()
        {
            if (GameStatics.gameOver)
            {
                GameStatics.spriteBatch.Draw(imgDeath, new Rectangle(carPosition.X, carPosition.Y, imgDeath.Width, imgDeath.Height), Color.White);
                GameStatics.mouseVisible = true;
                return;
            }
          
            if(!crash)
                GameStatics.spriteBatch.Draw(imgCar, new Rectangle(carPosition.X, carPosition.Y, imgCar.Width-5, imgCar.Height-13), Color.White);
            else
                GameStatics.spriteBatch.Draw(imgCar, new Rectangle(carPosition.X, carPosition.Y, imgCar.Width - 5, imgCar.Height - 13), Color.Gray);
                int x = 0;
                for (int i = 0; i < health; i++)
                {
                    GameStatics.spriteBatch.Draw(imgHealth, new Rectangle(1000 - x, 5, imgHealth.Width, imgHealth.Height), Color.White);
                    x += 20;

                }
            
            
        }
        public Point getPosition()
        {
            return carPosition;
        }
        public Rectangle getArea()
        {

            return new Rectangle(carPosition.X, carPosition.Y, imgCar.Width-5, imgCar.Height-13);
        }
        public void decreaseHealth()
        {
            crash = true;
            crashTime = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds+3;
            health--;
            if (health == 0)
            {
                GameStatics.gameOver = true;
                return;
            }
            carPosition = new Point(485, 600);
            fuel = 200;
        }
        public void incHealth()
        {
            health++;
        }
        public bool Crashed()
        {
            return crash;
        }
        public int getFuel()
        {
            return fuel;
        }
        public void incFuel()
        {
            fuel += 100;
        }

    }
 
}
