﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Racer
{
    class barrier
    {
        int length;
        Texture2D imgBarrier;
        Point[] positions;
        int[] decalY;
        double animElapsed = 0;
        public barrier()
        {
            Random rnd = new Random();
            length = rnd.Next(3, 11);
            positions = new Point[length];
            decalY = new int[length];
            imgBarrier = GameStatics.content.Load<Texture2D>("Sprites/barrier");
            int nextPosition = 0;
            for (int i = 0; i < length; i++)
            {
                positions[i] = new Point(503, -500+nextPosition);
                nextPosition -= 96;
                decalY[i] = positions[i].Y;

            }
        
        }
        public void Update(int speed)
        {
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                for (int i = 0; i < decalY.Length; i++)
                {
                    decalY[i] += speed;
                    positions[i].Y = decalY[i];
                }
            }

        }
        public void Draw()
        {
            for (int i = 0; i < length; i++)
            {
                GameStatics.spriteBatch.Draw(imgBarrier, new Rectangle(positions[i].X, positions[i].Y, imgBarrier.Width+10, imgBarrier.Height), Color.White);

            }
        }
        public Rectangle getArea()
        {
            return new Rectangle(positions[length-1].X, positions[length-1].Y, imgBarrier.Width+10, imgBarrier.Height * length);
        }
    }
}
