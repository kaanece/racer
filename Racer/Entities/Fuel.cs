﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Racer
{
    class Fuel
    {
        Point fuelPosition;
        int decalY;
        double animElapsed = 0;
        Texture2D imgFuel;
        public Fuel()
        {
            Random rnd = new Random();
            int line = rnd.Next(0, 2);
            if (line == 0)
                fuelPosition = new Point(300, -500);
            else
                fuelPosition = new Point(650, -500);
            decalY = fuelPosition.Y;
            imgFuel = GameStatics.content.Load<Texture2D>("Sprites/fuel");
        }
        public void Update(int speed)
        {
          
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                decalY += speed;
            }
            fuelPosition.Y = decalY;
        }
        public void Draw()
        {
            GameStatics.spriteBatch.Draw(imgFuel, new Rectangle(fuelPosition.X, fuelPosition.Y, imgFuel.Width, imgFuel.Height), Color.White);
        }
        public Rectangle getArea()
        {
            return new Rectangle(fuelPosition.X, fuelPosition.Y, imgFuel.Width, imgFuel.Height);
        }

    }
}
