﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Racer
{
    class health
    {
        Point healthPosition;
        Texture2D imgHealth;
        int decalY;
        double animElapsed = 0;
        public health()
        {
            Random rnd = new Random();
            int line = rnd.Next(0, 2);
            if (line == 0)
                healthPosition = new Point(300, -500);
            else
                healthPosition = new Point(650, -500);
            imgHealth = GameStatics.content.Load<Texture2D>("Sprites/health");
            decalY = healthPosition.Y;
        }
        public void Update(int speed)
        {
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                decalY += speed;
            }
            healthPosition.Y = decalY;
        }
        public void Draw()
        {

            GameStatics.spriteBatch.Draw(imgHealth, new Rectangle(healthPosition.X, healthPosition.Y, imgHealth.Width+10, imgHealth.Height+10), Color.White);

        }
        public Rectangle getArea()
        {
            return new Rectangle(healthPosition.X, healthPosition.Y, imgHealth.Width+10, imgHealth.Height+10);
        }
    }
}
