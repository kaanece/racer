﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Racer
{
    
    class Car
    {
        Point carPosition;
        Texture2D carTexture;
        Texture2D containerTexture;
        int decalY;
        double animElapsed = 0;
        int lineType;
        public Car(Point pos,int lType,int carType)
        {
            Random rnd = new Random();
           
            if (carType == 0)
            {
                int textureType = rnd.Next(0, 4);
                carTexture = GameStatics.content.Load<Texture2D>("Sprites/little" + textureType.ToString());
            }
            else if (carType == 1)
            {
                int textureType = rnd.Next(0, 4);
                carTexture = GameStatics.content.Load<Texture2D>("Sprites/normal" + textureType.ToString());
            }
            else if (carType == 2)
            {
                int textureType = rnd.Next(0, 4);
                carTexture = GameStatics.content.Load<Texture2D>("Sprites/long" + textureType.ToString());
            }
            else if (carType == 3)
            {

                int textureType = rnd.Next(0, 2);
                carTexture = GameStatics.content.Load<Texture2D>("Sprites/truck" + textureType.ToString());
            }
            else if (carType == 4)
            {
                int textureType = rnd.Next(0, 1);
                carTexture = GameStatics.content.Load<Texture2D>("Sprites/truck" + textureType.ToString());
                textureType = rnd.Next(0, 3);
                containerTexture = GameStatics.content.Load<Texture2D>("Sprites/container" + textureType.ToString());

            }
            carPosition = pos;
            lineType = lType;
            decalY = carPosition.Y;
        }
        public void Update(int speed)
        {
            carPosition.Y = decalY;
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                if (lineType == 0)
                    decalY -= -1 * speed - 2;
                else
                    decalY -= -1*speed + 2;
            }

        }
        public void Draw()
        {
    

            if (lineType == 0)
            {
                
                GameStatics.spriteBatch.Draw(carTexture, new Rectangle(carPosition.X,decalY, carTexture.Width, carTexture.Height), Color.White);
                if (containerTexture != null)
                {
                    GameStatics.spriteBatch.Draw(containerTexture, new Rectangle(carPosition.X-5, decalY - 220, containerTexture.Width, containerTexture.Height), Color.White);
                }
            }
            else
            {
                GameStatics.spriteBatch.End();
                GameStatics.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, DepthStencilState.None, RasterizerState.CullCounterClockwise, null);
                GameStatics.spriteBatch.Draw(carTexture, new Vector2(carPosition.X, decalY), null, Color.White, (float)Math.PI, new Vector2(carTexture.Width, carTexture.Height), 1, SpriteEffects.None, 0);
                if (containerTexture != null)
                {
                    GameStatics.spriteBatch.Draw(containerTexture, new Vector2(carPosition.X-3, decalY+80), null, Color.White, (float)Math.PI, new Vector2(containerTexture.Width, containerTexture.Height), 1, SpriteEffects.None, 0);

                }
                GameStatics.spriteBatch.End();
                GameStatics.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, null, null);
            }

        }
        public Point getPosition()
        {
            return carPosition;
        }
        public Rectangle getArea()
        {
            Rectangle area = new Rectangle(carPosition.X, carPosition.Y, carTexture.Width, carTexture.Height);
            if (containerTexture != null)
            {
                if(lineType==0)
                {
                area.Y -= containerTexture.Height;
                area.Height += containerTexture.Height;
                }
                else
                {
               
                area.Height += containerTexture.Height-70;
                }
            }

            return area;
        }
    }
}
