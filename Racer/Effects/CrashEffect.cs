﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Racer
{
    class CrashEffect
    {
        Point effectPosition;
        Texture2D imgEffect;
        int decalY;
        double animElapsed=0;
        int removeTime;
        public CrashEffect(Point crashPosition)
        {
            Random rnd = new Random();
            int crashType=rnd.Next(0, 4);
            imgEffect = GameStatics.content.Load<Texture2D>("Sprites/crash" + crashType.ToString());
            effectPosition = crashPosition;
            removeTime =(int) GameStatics.gameTime.TotalGameTime.TotalSeconds + 2;
            decalY = effectPosition.Y;
        }
        public void Update(int speed)
        {
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                decalY += speed;
            }
            effectPosition.Y = decalY;
        }
        public void Draw()
        {
            GameStatics.spriteBatch.Draw(imgEffect, new Rectangle(effectPosition.X, effectPosition.Y, imgEffect.Width+30, imgEffect.Height+30), Color.White);

        }
        public int  getRemoveTime()
        {
            return removeTime;
        }

    }
}
