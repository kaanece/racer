﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Racer
{
    class MainMenu:Screen
    {
        Texture2D imgBackground;
        Texture2D imgMenu;
        SpriteFont font;
        bool startMouseHover;
        bool exitMouseHover;
        SoundEffect buttonClickSound;
        public MainMenu()
        {
            startMouseHover=false;
             exitMouseHover=false;
            

        }
        public override void LoadContent()
        {
            base.LoadContent();
            imgBackground = GameStatics.content.Load<Texture2D>("Sprites/menubackground");
            imgMenu = GameStatics.content.Load<Texture2D>("Sprites/menu");
            font = GameStatics.content.Load<SpriteFont>("Fonts/font");
            buttonClickSound = GameStatics.content.Load<SoundEffect>("Sounds/buttonclick");


            
        }
        public override void Update()
        {
            Rectangle mouseRectangle = new Rectangle(Mouse.GetState().Position.X,Mouse.GetState().Position.Y,1,1);
            if (mouseRectangle.Intersects(new Rectangle(365, 290, imgMenu.Width, imgMenu.Height)))
            {
                startMouseHover = true;
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    buttonClickSound.Play();
                    GameStatics.currentScreen = new CarSelection();
                    GameStatics.currentScreen.LoadContent();
                }
            }
            else 
                startMouseHover=false;
            if (mouseRectangle.Intersects(new Rectangle(365, 360, imgMenu.Width, imgMenu.Height)))
            {
                exitMouseHover = true;
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    buttonClickSound.Play();
                    GameStatics.exit = true;
                }

            }
            else
                exitMouseHover = false;
          
              
            base.Update();
        }
        public override void Draw()
        {
            base.Draw();
            GameStatics.spriteBatch.Begin();
            GameStatics.spriteBatch.Draw(imgBackground, new Rectangle(0, 0, 1024, 768), Color.White);
          GameStatics.spriteBatch.Draw(imgMenu,new Rectangle(365,290,imgMenu.Width,imgMenu.Height),Color.White);
          GameStatics.spriteBatch.Draw(imgMenu, new Rectangle(365, 360, imgMenu.Width, imgMenu.Height), Color.White);
            if(startMouseHover==true)
            
                GameStatics.spriteBatch.DrawString(font, "Start New Game", new Vector2(400, 300), Color.Red);
            else
                GameStatics.spriteBatch.DrawString(font, "Start New Game", new Vector2(400, 300), Color.White);
            if(exitMouseHover==true)

                GameStatics.spriteBatch.DrawString(font, "Exit", new Vector2(490, 370), Color.Red);
            else
                GameStatics.spriteBatch.DrawString(font, "Exit", new Vector2(490, 370), Color.White);
          
            GameStatics.spriteBatch.End();
        }
    }
    
}
