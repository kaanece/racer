﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace Racer
{
    class CarSelection:Screen
    {
        Texture2D imgBackground;
        Texture2D carBorder;
        Texture2D mainCar0;
        Texture2D mainCar1;
        Texture2D mainCar2;
        SpriteFont font;
        bool startHover = false;
        bool maincar0Hover = false;
        bool maincar1Hover = false;
        bool maincar2Hover = false;
        SoundEffect buttonClickSound;
        public CarSelection()
        {

        }
        public override void LoadContent()
        {
            base.LoadContent();
            imgBackground = GameStatics.content.Load<Texture2D>("Sprites/menubackground");
            carBorder = GameStatics.content.Load<Texture2D>("Sprites/carBorder");
            mainCar0 = GameStatics.content.Load<Texture2D>("Sprites/maincar0");
            mainCar1 = GameStatics.content.Load<Texture2D>("Sprites/maincar1");
            mainCar2 = GameStatics.content.Load<Texture2D>("Sprites/maincar2");
            font = GameStatics.content.Load<SpriteFont>("Fonts/font");
            buttonClickSound = GameStatics.content.Load<SoundEffect>("Sounds/buttonclick");
        }
        public override void Update()
        {
            Rectangle mouseRectangle = new Rectangle(Mouse.GetState().Position.X, Mouse.GetState().Position.Y, 1, 1);
            if(mouseRectangle.Intersects(new Rectangle(300,100,carBorder.Width,carBorder.Height)))
            {
                maincar0Hover=true;
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    buttonClickSound.Play();
                    GameStatics.maincar = 0;
                }
            }
            else
                maincar0Hover=false;
            if(mouseRectangle.Intersects( new Rectangle(510, 110, mainCar1.Width, mainCar1.Height)))
            {
                maincar1Hover=true;
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    buttonClickSound.Play();
                    GameStatics.maincar = 1;
                }
            }
            else
                maincar1Hover=false;
             if(mouseRectangle.Intersects( new Rectangle(710, 110, mainCar2.Width, mainCar2.Height)))
            {
                maincar2Hover=true;
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    buttonClickSound.Play();
                    GameStatics.maincar = 2;
                }
            }
            else
                maincar2Hover=false;
             if (GameStatics.maincar != -1 && mouseRectangle.Intersects(new Rectangle(490, 250, 80, 20)))
             {
                 startHover = true;
                 if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                 {
                     buttonClickSound.Play();
                     GameStatics.currentScreen = new GameScreen();
                     GameStatics.currentScreen.LoadContent();
                 }
             }
             else
                 startHover = false;

            base.Update();
        }
        public override void Draw()
        {
            GameStatics.spriteBatch.Begin();
            GameStatics.spriteBatch.Draw(imgBackground, new Rectangle(0, 0, 1024, 768), Color.White);
            GameStatics.spriteBatch.DrawString(font,"Select Your Car",new Vector2(410,25),Color.White);
           
            if (maincar0Hover || GameStatics.maincar==0)
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(300, 100, carBorder.Width+10, carBorder.Height+10), Color.White);
                GameStatics.spriteBatch.Draw(mainCar0, new Rectangle(310, 110, mainCar0.Width + 10, mainCar0.Height + 10), Color.White);
            }
            else
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(300, 100, carBorder.Width, carBorder.Height), Color.White);
                GameStatics.spriteBatch.Draw(mainCar0, new Rectangle(310, 110, mainCar0.Width, mainCar0.Height), Color.White);
            }
            if (maincar1Hover || GameStatics.maincar==1)
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(500, 100, carBorder.Width+10, carBorder.Height+10), Color.White);
                GameStatics.spriteBatch.Draw(mainCar1, new Rectangle(510, 110, mainCar1.Width+10, mainCar1.Height+10), Color.White);
            }
            else
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(500, 100, carBorder.Width, carBorder.Height), Color.White);
                GameStatics.spriteBatch.Draw(mainCar1, new Rectangle(510, 110, mainCar1.Width, mainCar1.Height), Color.White);
            }
            if (maincar2Hover || GameStatics.maincar==2)
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(700, 100, carBorder.Width+10, carBorder.Height+10), Color.White);
                GameStatics.spriteBatch.Draw(mainCar2, new Rectangle(710, 110, mainCar2.Width+10, mainCar2.Height+10), Color.White);
            }
            else
            {
                GameStatics.spriteBatch.Draw(carBorder, new Rectangle(700, 100, carBorder.Width, carBorder.Height), Color.White);
                GameStatics.spriteBatch.Draw(mainCar2, new Rectangle(710, 110, mainCar2.Width, mainCar2.Height), Color.White);
            }
            if (GameStatics.maincar != -1)
            {
                if(startHover)
                    GameStatics.spriteBatch.DrawString(font, "Start!", new Vector2(490, 250), Color.Red);
                else
                GameStatics.spriteBatch.DrawString(font, "Start!", new Vector2(490, 250), Color.White);
            }
            GameStatics.spriteBatch.End();
            base.Draw();
        }
    }
}
