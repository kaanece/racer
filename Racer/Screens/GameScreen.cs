﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace Racer
{
    class GameScreen:Screen
    {
        Texture2D imgBackground;
        Texture2D imgRoad;
        Texture2D imgScoreBoard;
        Texture2D imgFuelBar;
        Texture2D imgGameOver;
        int decalY;
        double animElapsed=0;
        PlayerCar playerCar;
        int speed;
        List<Car> Cars = new List<Car>();
        List<House> Houses = new List<House>();
        List<CrashEffect> crashEffects = new List<CrashEffect>();
        List<barrier> barriers = new List<barrier>();
        List<Coin> coins = new List<Coin>();
        Fuel fuel;
        health health;
        double nextCar;
        double nextHouse;
        double nextFuel;
        double nextHealth;
        double nextBarrier;
        double nextCoins;
        SpriteFont gameFont;
        int gameSeconds=1;
        bool roadOutSide = false;
        bool startHover = false;
        bool exitHover = false;
        SoundEffect coinSound;
        SoundEffect carCrashSound;
        SoundEffect fuelSound;
        SoundEffect healthSound;
        SoundEffect buttonClickSound;
        public GameScreen()
        {
            GameStatics.gameScore = 0;
            GameStatics.gameTime.TotalGameTime = new TimeSpan(0, 0, 0);
            playerCar = new PlayerCar();
            Random rnd = new Random();
            nextCar = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + 2;
            nextHouse = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + 2;
            nextFuel = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds+rnd.Next(10, 20);
            nextHealth = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds+rnd.Next(15, 25);
            nextBarrier = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds+rnd.Next(5, 15);
            nextCoins = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(5, 15);

        }
        public override void LoadContent()
        {
            imgBackground = GameStatics.content.Load<Texture2D>("Sprites/background");
            imgRoad = GameStatics.content.Load<Texture2D>("Sprites/road");
            imgScoreBoard = GameStatics.content.Load<Texture2D>("Sprites/scoreBoard");
            imgFuelBar = GameStatics.content.Load<Texture2D>("Sprites/fuelBar");
            imgGameOver = GameStatics.content.Load<Texture2D>("Sprites/gameover");
            gameFont = GameStatics.content.Load<SpriteFont>("Fonts/font");
            coinSound = GameStatics.content.Load<SoundEffect>("Sounds/coinsound");
            carCrashSound = GameStatics.content.Load<SoundEffect>("Sounds/carcrash");
            fuelSound = GameStatics.content.Load<SoundEffect>("Sounds/fuelsound");
            healthSound = GameStatics.content.Load<SoundEffect>("Sounds/healthsound");
            buttonClickSound = GameStatics.content.Load<SoundEffect>("Sounds/buttonclick");
            speed = 5;
            base.LoadContent();
        }
        public override void Update()
        {
            if (GameStatics.gameOver)
            {
                
                Rectangle mouseRectangle = new Rectangle(Mouse.GetState().Position.X, Mouse.GetState().Position.Y, 1, 1);
                if (mouseRectangle.Intersects(new Rectangle(450, 400, 110, 20)))
                {
                    startHover = true;
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        buttonClickSound.Play();
                        GameStatics.currentScreen = new GameScreen();
                        GameStatics.currentScreen.LoadContent();
                        GameStatics.gameOver = false;
                       
                    }
                }
                else
                    startHover = false;
                if (mouseRectangle.Intersects(new Rectangle(475, 450, 60, 20)))
                {
                    exitHover = true;
                    if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        buttonClickSound.Play();
                        GameStatics.exit = true;
                    }
            
                }
                else
                    exitHover = false;
                return;
            }
            if (fuel != null)
                fuel.Update(speed);
            if (health != null)
                health.Update(speed);
            for (int i = 0; i < coins.Count; i++)
            {

                coins[i].Update(speed);
                if (!playerCar.Crashed())
                if(coins[i].getArea().Intersects(playerCar.getArea()))
                {
                    coinSound.Play();
                    GameStatics.gameScore+=10;
                    coins[i] = null;
                    coins.Remove(coins[i]);
                    break;
                }
                if (coins[i].getArea().Y > 1000)
                {
                    coins[i] = null;
                    coins.Remove(coins[i]);
                }
            }

            playerCar.Update();
            for (int i = 0; i < Cars.Count; i++)
            {
                Cars[i].Update(speed);
                if (!playerCar.Crashed())
                {
                    if (Cars[i].getArea().Intersects(playerCar.getArea()))
                    {
                        carCrashSound.Play();
                        CrashEffect newCrashEffect = new CrashEffect(playerCar.getPosition());
                        crashEffects.Add(newCrashEffect);
                        playerCar.decreaseHealth();
                        GameStatics.gameScore -= 50;
                        break;
                    }
                }
                if (Cars[i].getPosition().Y > 1000)
                {
                    Cars[i] = null;
                    Cars.Remove(Cars[i]);
                }
                
            }
            for (int i = 0; i < Houses.Count; i++)
            {

                Houses[i].Update(speed);
                if (!playerCar.Crashed())
                {
                    if (Houses[i].getArea().Intersects(playerCar.getArea()))
                    {
                        carCrashSound.Play();
                        CrashEffect newCrashEffect = new CrashEffect(playerCar.getPosition());
                        crashEffects.Add(newCrashEffect);
                        playerCar.decreaseHealth();
                        GameStatics.gameScore -= 50;
                        break;
                    }
                }
               

            }
            for (int i = 0; i < crashEffects.Count; i++)
            {
                crashEffects[i].Update(speed);
                if (crashEffects[i].getRemoveTime() == (int)GameStatics.gameTime.TotalGameTime.TotalSeconds)
                {
                    crashEffects[i] = null;
                    crashEffects.Remove(crashEffects[i]);
                }
            }


            if (playerCar.getPosition().X < 250 || playerCar.getPosition().X > 724)
            {
                speed = 1;
                GameStatics.gameScore -= 0.1;
                roadOutSide = true;
            }
            else
            {
                speed = 5;
                roadOutSide = false;
            }
            animElapsed += GameStatics.gameTime.TotalGameTime.TotalMilliseconds;
            if (animElapsed > 10)
            {
                decalY-=speed;
            }
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextCar)
            {
                
                Random rnd = new Random();
                Point carPosition = new Point();
                int carType = rnd.Next(0, 5);
                int lineType = 0;
                int line = rnd.Next(1, 5);
                if (line == 1)
                {
                    carPosition = new Point(300, -500);
                    lineType = 0;
                }
                else if (line == 2)
                {
                    carPosition = new Point(400, -500);
                    lineType = 0;
                }
                else if (line == 3)
                {
                    carPosition = new Point(550, -500);
                    lineType = 1;
                }
                else if (line == 4)
                {
                    carPosition = new Point(650, -500);
                    lineType = 1;
                }
   
                bool empty = true;
                foreach (Car c in Cars)
                {
                   if( c.getArea().Intersects(new Rectangle(carPosition.X,carPosition.Y,c.getArea().Width,c.getArea().Height)))
                    {
                        empty=false;
                        break;
                    }
                }
                if (empty == true)
                {
                    Car newCar = new Car(carPosition, lineType,carType);
                    Cars.Add(newCar);
                    nextCar = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(0, 2);
                }
                else
                nextCar =(int) GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(0, 2);
            }

            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextHouse)
            {
                Random rnd = new Random();
                int roadSide = rnd.Next(0, 2);
                Point housePosition;
                if (roadSide == 1)
                {
                    housePosition = new Point(rnd.Next(10, 120), -300);
                }
                else
                {
                    housePosition = new Point(rnd.Next(750, 900), -300);
                } 
                 bool empty = true;
                foreach (House h in Houses)
                {
                   if( h.getArea().Intersects(new Rectangle(housePosition.X,housePosition.Y,100,100)))
                    {
                        empty=false;
                        break;
                    }
                }
                if (empty == true)
                {
                    int houseType = rnd.Next(0, 35);
                    House newhouse = new House(housePosition);
                    Houses.Add(newhouse);
                    nextHouse = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(0, 2);
                }

            }
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextFuel)
            {
                fuel = new Fuel();
                Random rnd = new Random();
                nextFuel =(int)GameStatics.gameTime.TotalGameTime.TotalSeconds+ rnd.Next(10, 20);
            }
            if(fuel != null && playerCar.getArea().Intersects(fuel.getArea()))
            {
                if (!playerCar.Crashed())
                {
                    fuelSound.Play();
                    playerCar.incFuel();
                    fuel = null;
                }
            }
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextHealth)
            {
                health = new health();
                Random rnd = new Random();
                nextHealth = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(15, 25);
            }
            if (health != null && playerCar.getArea().Intersects(health.getArea()))
            {
                if (!playerCar.Crashed())
                {
                    healthSound.Play();
                    playerCar.incHealth();
                    health = null;
                }
            }
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextBarrier)
            {
                barrier b = new barrier();
                barriers.Add(b);
                Random rnd = new Random();
                nextBarrier = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(5, 15);
            }
            for (int i = 0; i < barriers.Count; i++)
            {
                
                barriers[i].Update(speed);
             
                if (!playerCar.Crashed())
                {
                    if (barriers[i].getArea().Intersects(playerCar.getArea()))
                    {
                        carCrashSound.Play();
                        CrashEffect newCrashEffect = new CrashEffect(playerCar.getPosition());
                        crashEffects.Add(newCrashEffect);
                        GameStatics.gameScore -= 50;
                        playerCar.decreaseHealth();
                    }
                }
                if (barriers[i].getArea().Y > 2500)
                {
                    barriers[i] = null;
                    barriers.Remove(barriers[i]);
                }
            }
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == nextCoins)
            {
                Random rnd = new Random();
                int startingLine = rnd.Next(0, 4);
                int linesize = rnd.Next(0, 3 - startingLine);
                int size = rnd.Next(5, 11);
                Point[][] positions = new Point[linesize+1][];
          
                for (int i = 0; i < positions.Length; i++)
                {
                    int lengthBetweenCoins = 0;
                    positions[i] = new Point[size];
                    for (int j = 0; j < size; j++)
                    {
                        
                        if(startingLine+i==0)
                        {
                            positions[i][j] = new Point(300, -500+lengthBetweenCoins);
                        }
                        else if (startingLine+i == 1)
                        {
                            positions[i][j] = new Point(400, -500 + lengthBetweenCoins);
                        }
                        else if (startingLine+i == 2)
                        {
                            positions[i][j] = new Point(550, -500 + lengthBetweenCoins);
                        }
                        else if (startingLine+i == 3)
                        {
                            positions[i][j] = new Point(650, -500 + lengthBetweenCoins);
                        }
                        lengthBetweenCoins -=50;
                        Coin c = new Coin(positions[i][j]);
                        coins.Add(c);
                    }

                }
                nextCoins = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + rnd.Next(5, 15);

            }
            if(!roadOutSide)
            if ((int)GameStatics.gameTime.TotalGameTime.TotalSeconds == gameSeconds)
                GameStatics.gameScore += 10;

            gameSeconds = (int)GameStatics.gameTime.TotalGameTime.TotalSeconds + 1;
            if (GameStatics.gameScore < 0)
                GameStatics.gameScore = 0;
            base.Update();
        }
        public override void Draw()
        {
          
            GameStatics.spriteBatch.Begin(SpriteSortMode.Deferred,BlendState.AlphaBlend,SamplerState.LinearWrap,null,null);
           
            GameStatics.spriteBatch.Draw(imgBackground,new Vector2(0,0), new Rectangle(0, decalY, 1024, 768), Color.White);
            GameStatics.spriteBatch.Draw(imgRoad,new Vector2(250,0), new Rectangle(250, decalY, imgRoad.Width, GameStatics.gameHeight), Color.White);
            foreach (Coin c in coins)
                c.Draw();
            if (fuel != null)
                fuel.Draw();
            if (health != null)
                health.Draw();
            foreach(Car c in Cars)
            {
                c.Draw();
            }
            foreach (House h in Houses)
            {
                h.Draw();
            }
            foreach (barrier b in barriers)
            {
                b.Draw();

            }
            playerCar.Draw();
            foreach (CrashEffect crashEffect in crashEffects)
            {
                crashEffect.Draw();
            }
            
            GameStatics.spriteBatch.Draw(imgScoreBoard, new Rectangle(770, 540, 300, 300), Color.White);
            GameStatics.spriteBatch.DrawString(gameFont, "Time:  " +(int) GameStatics.gameTime.TotalGameTime.TotalMinutes + ":" +(int) GameStatics.gameTime.TotalGameTime.Seconds+"\nScore: "+(int)GameStatics.gameScore, new Vector2(820, 650), Color.White);
           if(playerCar.getFuel()<100)
               GameStatics.spriteBatch.Draw(imgFuelBar, new Rectangle(790, 600, playerCar.getFuel(), imgFuelBar.Height), Color.Red);
            else
            GameStatics.spriteBatch.Draw(imgFuelBar,new Rectangle(790,600,playerCar.getFuel(),imgFuelBar.Height),Color.Green);
            GameStatics.spriteBatch.DrawString(gameFont, "Fuel", new Vector2(850, 560), Color.White);
            if (GameStatics.gameOver)
            {
                GameStatics.spriteBatch.Draw(imgGameOver, new Rectangle(350, 300, imgGameOver.Width, imgGameOver.Height), Color.White);
                if(startHover)
                 GameStatics.spriteBatch.DrawString(gameFont, "Restart", new Vector2(450, 400), Color.Red);
                    else
                GameStatics.spriteBatch.DrawString(gameFont, "Restart", new Vector2(450, 400), Color.White);
                if(exitHover)
                    GameStatics.spriteBatch.DrawString(gameFont, "Exit", new Vector2(475, 450), Color.Red);
                else
                GameStatics.spriteBatch.DrawString(gameFont, "Exit", new Vector2(475, 450), Color.White);
            }
            GameStatics.spriteBatch.End();
            base.Draw();
        }
    }
}
