﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Racer
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            GameStatics.content = Content;
            GameStatics.gDevice = GraphicsDevice;
            graphics.PreferredBackBufferWidth = GameStatics.gameWidth;
            graphics.PreferredBackBufferHeight = GameStatics.gameHeight;
            Window.Title = GameStatics.gameTitle;
            graphics.ApplyChanges();
        }

       
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            this.IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
 
            spriteBatch = new SpriteBatch(GraphicsDevice);
            GameStatics.spriteBatch = spriteBatch;
            GameStatics.currentScreen = new MainMenu();
            GameStatics.currentScreen.LoadContent();
         
        }

      
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

     
        protected override void Update(GameTime gameTime)
        {
            
            GameStatics.gameTime = gameTime;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (!GameStatics.mouseVisible)
                IsMouseVisible = false;
           GameStatics.currentScreen.Update();
            if (GameStatics.exit == true)
                Exit();
        
            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
           GameStatics.currentScreen.Draw();

            base.Draw(gameTime);
        }
    }
}
