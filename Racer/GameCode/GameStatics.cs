﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Racer
{
    class GameStatics
    {
        public static int gameWidth = 1024;
        public static int gameHeight = 768;
        public static string gameTitle = "Racer Game Created By Kaan ECE";
        public static GameTime gameTime;
        public static SpriteBatch spriteBatch;
        public static ContentManager content;
        public static GraphicsDevice gDevice;
        public static bool gameOver = false;
        public static double gameScore = 0;
        public static bool exit = false;
        public static int maincar = -1;
        public static Screen currentScreen;
        public static bool mouseVisible=true;

    }
}
